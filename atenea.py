#!/usr/bin/env python3

# Copyright (C) 2019 Oscar Benedito, Ernesto Lanchares, Ferran López
#
# This file is part of Atenea Updates Notifications (AUN).
#
# AUN is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AUN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AUN.  If not, see <https://www.gnu.org/licenses/>.

import os
import requests
import json
import time

def course_is_valid(token, courseid):
    return ('exception' not in get_course_docs(token, courseid))

def find_updates(token, time_difference, id):
    documents = []
    updates = get_updates(token, time_difference, id)
    if updates != []:
        course_docs = get_course_docs(token, id)
    for update in updates:
        doc = find_document(course_docs, update['id'])
        if doc['modname'] == 'resource':
            print()
            documents.append( (doc['name'],
                              doc['contents'][0]['filename'],
                              requests.get(
                                  doc['contents'][0]['fileurl'] + '&token=' + token
                              ).content) )
    return documents

def get_updates(token, time_difference, id):
    parameters = {
        'wstoken': token,
        'moodlewsrestformat': 'json',
        'wsfunction': 'core_course_get_updates_since',
        'courseid': id,
        'since': int(time.time()) - time_difference
    }
    response = requests.get('https://atenea.upc.edu/webservice/rest/server.php', params=parameters)
    return response.json()['instances']


def get_course_docs(token, id):
    parameters = {
        'wstoken': token,
        'moodlewsrestformat': 'json',
        'wsfunction': 'core_course_get_contents',
        'courseid': id,
    }
    return requests.get('https://atenea.upc.edu/webservice/rest/server.php', params=parameters).json()


def find_document(docs, doc_id):
    for module in docs:
        for doc in module['modules']:
            if doc['id'] == doc_id:
                return doc
