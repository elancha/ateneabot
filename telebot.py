from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext, CallbackQueryHandler
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram import ParseMode
from atenea import course_is_valid, find_updates
import pickle, threading, io

TIME_INTERVAL = 120

class is_handler:
        reg = {}

        def __init__(self, cmd, args=False, user_data=False, chat_data=True):
            self.cmd = cmd
            self.args = args
            self.user_data = user_data
            self.chat_data = chat_data

        def __call__(self, fnc):
            self.reg[self.cmd] = {'f': fnc, 'a': self.args, 'ud': self.user_data, 'cd': self.chat_data}
            return fnc

@is_handler('add', args = True)
def add(update: Update, context: CallbackContext):
    try:
        user_token = context.args[0]
        courseid = context.args[1]
        coursename = ' '.join(context.args[2:])

        if course_is_valid(user_token, courseid) and courseid not in course_info:
            course_info[courseid] = {
                'token': user_token,
                'name': coursename,
                'users': set()
            }
            context.bot.send_message(chat_id = update.effective_chat.id,
                text = 'Curso añadido 🥳')
        elif courseid in course_info:
            context.bot.send_message(chat_id = update.effective_chat.id,
                text = 'Ese curso ya existe, *PALOMO*.',
                parse_mode = ParseMode.MARKDOWN)
        else:
            context.bot.send_message(chat_id = update.effective_chat.id,
                text = 'Eso no está en Atenea, *PALOMO*.',
                parse_mode = ParseMode.MARKDOWN)
    except Exception as e:
        print(repr(e))
        context.bot.send_message(chat_id = update.effective_chat.id,
            text = 'Para añadir un nuevo texto, tienes que poner /add token id _nombre_',
            parse_mode = ParseMode.MARKDOWN)
        context.bot.send_message(chat_id = update.effective_chat.id,
            text = 'Para obtener token, ir a la página de Atenea, ir a arriba a la izquierda > \
                Preferencias > Claus de seguretat  y copiar la llave correspondiente a \
                "Moodle mobile web service"',
            parse_mode = ParseMode.MARKDOWN)
        context.bot.send_message(chat_id = update.effective_chat.id,
            text = 'Para obtener id, ir a la página de Atenea del curso y mirar en la URL \
                lo que pone ?id=*ID*',
            parse_mode = ParseMode.MARKDOWN)


@is_handler('start')
def start(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id,
        text='Para suscribirte a algún curso usa /subscribe y para añadir un curso nuevo usa /add')

@is_handler("subscribe")
def subscribe(update: Update, context: CallbackContext):

    keyboard = get_courses_as_keyboard(update.message.from_user['id'])
    markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('¿A qué cursos te quieres suscribir?',
                            reply_markup = markup)

def get_courses_as_keyboard(userid):
    return [ [InlineKeyboardButton(
                                ('✔️' if userid in course_info[courseid]['users'] else '❌') +
                                course_info[courseid]['name'],
                                callback_data=courseid)]
              for courseid in course_info]

def subscribe_course(update: Update, context: CallbackContext):
    query = update.callback_query
    courseid = query.data
    userid = query.from_user['id']
    if userid in course_info[courseid]['users']:
        course_info[courseid]['users'].remove(userid)
    else:
        course_info[courseid]['users'].add(userid)

    keyboard = get_courses_as_keyboard(userid)
    markup = InlineKeyboardMarkup(keyboard)

    query.edit_message_text('¿A qué cursos te quieres suscribir?',
                              reply_markup = markup)

def updates():
    threading.Timer(TIME_INTERVAL, updates).start()
    for courseid in course_info:
        docs = find_updates(course_info[courseid]['token'], int(TIME_INTERVAL*1.125), courseid)
        for name, filename, doc in docs:
            file_id = 0
            for user in course_info[courseid]['users']:
                if file_id == 0:
                    message = dispatcher.bot.send_document(user,
                        io.BytesIO(doc),
                        filename = filename,
                        caption = 'Se ha añadido el documento "{}" al curso "{}"'.format(
                            name, course_info[courseid]['name']))
                    file_id = message['document']['file_id']
                else:
                    dispatcher.bot.send_document(user,
                        file_id,
                        filename = filename,
                        caption = 'Se ha añadido el documento "{}" al curso "{}"'.format(
                            name, course_info[courseid]['name']
                        ))



if __name__ == '__main__':

    # Load Data

    with open('telegram_token.txt', 'r') as f:
        token = f.read().strip()
    try:
        with open('course_info.pickle', 'rb') as f:
            course_info = pickle.load(f)
    except FileNotFoundError:
        course_info = {}

    # Start telegram bot
    updater = Updater(token = token, use_context = True)
    dispatcher = updater.dispatcher

    for k, v in is_handler.reg.items():
        func = v['f']
        dispatcher.add_handler(
            CommandHandler(k, func, pass_args=v['a'],
            pass_user_data=v['ud'], pass_chat_data=v['cd']))
    dispatcher.add_handler(CallbackQueryHandler(subscribe_course))



    # Start polling
    updater.start_polling()
    updates()
    print('Bot running')
        
